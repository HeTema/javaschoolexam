package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        int length = inputNumbers.size();
        int[][] arr=new int[0][0];
        length=checkSize(length);
        try{
            if(length!=0){
                arr = new int[length][length*2-1];
                Collections.sort(inputNumbers);
                int counter=0;
                for(int i=0; i<length; i++){
                    for(int j=(((length)*2-1)/2)-i; j<length*2-1-(length*2-1)/2+i; j+=2){
                        arr[i][j]=inputNumbers.get(counter++);
                    }
                }
            }else throw new CannotBuildPyramidException();
        }catch(Throwable e){
            throw new CannotBuildPyramidException();
        }
        return arr;
    }

    public int checkSize(int length){
        if(length==0)return 0;
        int i=1;
        int correctLength=1;
        while(length!=correctLength){
            correctLength+=++i;
        }
        return i;
    }
}
