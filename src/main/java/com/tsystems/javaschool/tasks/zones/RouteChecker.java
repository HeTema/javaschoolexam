package com.tsystems.javaschool.tasks.zones;

import java.util.Iterator;
import java.util.List;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */
    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        Iterator<Integer> it = requestedZoneIds.iterator();
        Integer current=it.next();
        Zone currentZone=null;
        for(Zone zone : zoneState){
            if(zone.getId()==current){
                currentZone=zone;
                break;
            }
        }
        try{
            do{
                Integer next = it.next();
                Zone nextZone=null;
                for(Zone zone : zoneState){
                    if(zone.getId()==next){
                        nextZone=zone;
                        break;
                    }
                }
                /*
                last if-expression - if only cycled pass is allowed
                meaning 5-1-2-3 IS allowed
                because 1-2-3-5 not allowed
                */
                if(!currentZone.getNeighbours().contains(nextZone.getId())
                        &&!nextZone.getNeighbours().contains(currentZone.getId())
                        &&!nextZone.getNeighbours().contains(requestedZoneIds.get(0))){
                    return false;
                }
                currentZone=nextZone;
            }while(it.hasNext());
        }catch(Throwable e){
            return false;
        }
        return true;
    }
}

